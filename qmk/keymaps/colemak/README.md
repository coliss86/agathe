# Coliss86 Keymap

I use [COLEMAK mod DH](https://colemakmods.github.io/mod-dh/) as a layout and have moved some keys to place the most frequently used French accents on the base layer, such as é in the place of y and à in the place of w.
The keyboard is configured to send Azerty codes with the most frequently used French accents on the base layer (see the explanation below)."

![colemak mod dh with accent](doc/layout.jpg)

### But why Azerty keycodes ⁉️

This keyboard is intended to be used as a replacement for an Azerty keyboard on French computers without any modifications on the OS side.

I use a Macbook with an integrated Azerty keyboard. I often connect an external PC keyboard to it, but OS X does not map it to a PC layout without any action. You have to go to `System Preferences` > `Keyboard` > `Input Sources` > `Add the layout "French PC"`. Then, every time I want to use the integrated or external keyboard, I have to change the layout. It is a shame that OS X is not able to handle a layout per keyboard.
To solve this issue, I used [Karabiner](https://karabiner-elements.pqrs.org/) to map an Azerty external PC keyboard to the OS X layout in order to not change anything on OS X. Here is my [config](https://gitlab.com/-/snippets/2148079).

## Features 🔮

Special features of these layouts:
- [Space cadet shift](https://docs.qmk.fm/#/feature_space_cadet) (Orange Shift Key): tap shift: `{`, hold: `shift`
- [Leader key](https://docs.qmk.fm/#/feature_leader_key) (Purple Leader Key): bunch of shortcuts
- [Tap dance](https://docs.qmk.fm/#/feature_tap_dance): one tap: `:-)`, two taps: `;-)`, three taps: `:muscle:`...
- LED flash when a wrong combo is typed or when a mapping on a layer is `xxxxxxx`: see the [`flash_leds() function` in keymap.c](keymap.c)
- A rotary is used to [adjust the sound volume and when clicked, it acts as a scroll wheel](https://njkyu.com/2021/05/01/rotary-encoder-qmk/)
- Workaround for Ubuntu 21.04 which doesn't respond to keys pressed too quickly, such as pressing <kbd>shift + 1</kbd> and then releasing <kbd>shift</kbd>

## Layouts 👗👔

*Base layer*: Colemak-DH mapping

![Colemak-DH Layout](doc/layout_colemak-base.png)

*French accents*: çîïëùûô, euro €, F1-F9:

![Accent Layout](doc/layout_colemak-accent.png)

*Function layout*: Atmega Reset, screen brightness +/-, RGB Control: next effect, increase / decrease brightness, change effet and color, toggle on / off:

![Func Layout](doc/layout_colemak-func.png)

## Slide switch

<img src="../../../doc/slide-switch.jpg" alt="Slide-switch" width="500"/>

A slide switch is connected between `B2`, `B3`, and `GND`.
It allows you to change the layout by moving the switch.

![Slide](../../../doc/dip.jpg)

The position nearest to the USB-C connector deactivates the `_PC` layout, while the second position activates the `_PC` layout. This is very useful when I switch computers :muscle:.

## Compile and flash

### Keyboard shortcut

Press: <kbd>|</kbd> + <kbd>Click on rotary</kbd>.

See the definition of `RESET` in the `_FUNC` layer and its usage in [the function `process_record_user()` in keymap.c](keymap.c).

### CLI

```
qmk flash -kb agathe -km colemak
```
and press the reset button on the back of the keyboard, located to the left of the USB-C connector on the PCB. See the picture below.

<img src="doc/reset_button.jpg" alt="Reset button" width="400"/>
