DIP_SWITCH_ENABLE = yes  # Enable dip switch (hardware slide switch)

LEADER_ENABLE = yes # Leader key

RGBLIGHT_ENABLE = yes  # Enable keyboard RGB underglow

ENCODER_ENABLE = yes # Enable encoder
MOUSEKEY_ENABLE = yes # mouse for wheel up / down
