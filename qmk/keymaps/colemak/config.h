/*
Copyright 2021 Coliss86

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// RGB light
#if defined(RGBLIGHT_ENABLE)

  #define RGBLIGHT_ANIMATIONS

  #define RGBLIGHT_EFFECT_BREATHING

  #define RGBLIGHT_DEFAULT_HUE 28 // Orange
  #define RGBLIGHT_DEFAULT_SAT 255
  #define RGBLIGHT_DEFAULT_VAL 120
  #define RGBLIGHT_DEFAULT_SPD 10

  #define RGBLIGHT_EFFECT_TWINKLE_LIFE 200
  #define RGBLIGHT_EFFECT_TWINKLE_PROBABILITY 32000

  #define RGBLIGHT_DEFAULT_MODE RGBLIGHT_MODE_RAINBOW_SWIRL + 2

#endif

#define DIP_SWITCH_PINS { B2 }

#define ENCODERS_PAD_A { B7 }
#define ENCODERS_PAD_B { B3 }

#define LEADER_TIMEOUT 700

#define NO_ACTION_ONESHOT
