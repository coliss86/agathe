/*
Copyright 2021 Coliss86

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include QMK_KEYBOARD_H

#include "keymap_french.h"
#include "sendstring_french.h"
#include "string.h"

#define RGB_LAYER_ACK_DURATION 300
#define DELAY_TAP_CODE 10

enum layers {
    _MACOS = 0,
    _PC,
    _ACCENT,
    _FUNC,
};

// Macro Declarations
enum custom_keycodes {
    EMPTY = SAFE_RANGE,
    A_CIRC, A_GRAVE, E_ACUTE, E_GRAVE, E_CIRC, E_TREMA, U_CIRC, I_CIRC, I_TREMA, O_CIRC, C_CEDI, MICRO, EURO, // accent
    DOT, COMMA, SCLN, PIPE, QUOT, SLASH, MINS, EQUAL, TILDE, LBRC, RBRC, // punctuation
    ALT_TAB, GRA_ESC, MAKE, ENC_SW, VERSION, INVALID, // utils
    NUM_1, NUM_2, NUM_3, NUM_4, NUM_5, NUM_6, NUM_7, NUM_8, NUM_9, NUM_0, // numbers
};

// Tap Dance declarations
enum tap_dance {
    HOME_COMBO,
    SMILEY_HAPPY,
    SMILEY_CRY,
    LSHIFT_COMBO,
    RSHIFT_COMBO,
};

enum encoder_mode {
  VOLUME,
  SCROLL,
  SCREEN_LIGHT,

  // Track the amount of modes
  _NUM_MODES,
};

#define LR_FUNC     LT(_FUNC, EMPTY) // this used instead MT() to enter in process_record_user()
#define LR_ACC      LT(_ACCENT, KC_SPC)
#define TD_HAPP     TD(SMILEY_HAPPY)
#define TD_CRY      TD(SMILEY_CRY)
#define TD_HOME     TD(HOME_COMBO)
#define TD_LSFT     TD(LSHIFT_COMBO)
#define TD_RSFT     TD(RSHIFT_COMBO)
#define xxxxxxx     INVALID

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

[_MACOS] = LAYOUT(
// ,--------+--------+--------+--------+--------+--------+--------.    |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    GRA_ESC, NUM_1,   NUM_2,   NUM_3,   NUM_4,   NUM_5,   MINS,         NUM_6,   NUM_7,   NUM_8,   NUM_9,   NUM_0,   EQUAL,   KC_BSPC,          ENC_SW,
// |--------+--------+--------+--------+--------+--------+--------'    |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    KC_TAB , FR_Q,    FR_W,    FR_F,    FR_P,    FR_B,                  A_GRAVE, FR_J,    FR_L,    FR_U,    FR_Y,    SCLN,    QUOT,    LR_FUNC, TD_HAPP,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    KC_BSPC, FR_A,    FR_R,    FR_S,    FR_T,    FR_G,                  E_ACUTE, FR_M,    FR_N,    FR_E,    FR_I,    FR_O,    KC_ENT,           TD_HOME,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------'
    TD_LSFT, FR_Z,    FR_X,    FR_C,    FR_D,    FR_V,                  E_GRAVE, SLASH,   FR_K,    FR_H,    COMMA,   DOT,     TD_RSFT, KC_UP,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    KC_LCTL,          KC_LGUI,      LR_ACC,      KC_LALT,                       LR_ACC,         KC_LEAD,                      KC_LEFT, KC_DOWN, KC_RGHT
// '--------'        '--------+-----------------+--------'             '-----------------------+--------'                    '--------+--------+--------'
),

[_ACCENT] = LAYOUT(
// ,--------+--------+--------+--------+--------+--------+--------.    |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    _______, KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   _______,      KC_F6,   KC_F7,   KC_F8,   KC_F9,   FR_DEG,  _______, _______,          _______,
// |--------+--------+--------+--------+--------+--------+--------'    |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______, _______, _______, EURO,    EURO,    _______,               _______, _______, U_CIRC,  FR_UGRV, _______, _______, _______, _______, _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    KC_DEL,  A_CIRC,  _______, _______, _______, _______,               C_CEDI,  MICRO,   TILDE,   E_CIRC,  I_CIRC,  O_CIRC,  _______,          _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------'
    LBRC,    _______, KC_MUTE, C_CEDI,  FR_DEG,  _______,               E_CIRC,  _______, _______, E_TREMA, I_TREMA, _______, RBRC,    KC_PGUP,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    _______,          _______, _______,          _______,                       _______,        _______,                      KC_HOME, KC_PGDN, KC_END
// '--------'        '--------+-----------------+--------'             '-----------------------+--------'                    '--------+--------+--------'
),

[_PC] = LAYOUT(
// ,--------+--------+--------+--------+--------+--------+--------.    |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    _______, _______, _______, _______, _______, _______, _______,      _______, _______, _______, _______, _______, _______, _______,          _______,
// |--------+--------+--------+--------+--------+--------+--------'    |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    ALT_TAB, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______, _______, _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______,          _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------'
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______, _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    KC_LCTL,          KC_LCTL, _______,          KC_LGUI,                       _______,        _______,                       _______, _______, _______
// '--------'        '--------+-----------------+--------'             '-----------------------+--------'                    '--------+--------+--------'
),

[_FUNC] = LAYOUT(
// ,--------+--------+--------+--------+--------+--------+--------.    |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,      xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,          MAKE,
// |--------+--------+--------+--------+--------+--------+--------'    |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx, KC_BRIU, xxxxxxx, xxxxxxx, RGB_MOD, RGB_TOG,               RGB_M_P, RGB_M_B, RGB_M_R, RGB_M_SW,xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, TD_CRY,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx, KC_BRID, RGB_SPI, RGB_HUI, RGB_VAI, RGB_SAI,               RGB_M_SN,RGB_M_K, RGB_M_X, RGB_M_G, xxxxxxx, xxxxxxx, xxxxxxx,          VERSION,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------'
    xxxxxxx, xxxxxxx, RGB_SPD, RGB_HUD, RGB_VAD, RGB_SAD,               xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    xxxxxxx,          xxxxxxx, xxxxxxx,          xxxxxxx,                       xxxxxxx,        xxxxxxx,                      xxxxxxx, xxxxxxx, xxxxxxx
// '--------'        '--------+-----------------+--------'             '-----------------------+--------'                    '--------+--------+--------'
),

/*
[NAME] = LAYOUT(
// ,--------+--------+--------+--------+--------+--------+--------.    |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    _______, _______, _______, _______, _______, _______, _______,      _______, _______, _______, _______, _______, _______, _______,          _______,
// |--------+--------+--------+--------+--------+--------+--------'    |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______, _______, _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______,          _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------'
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______, _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    xxxxxxx,          xxxxxxx, xxxxxxx,          xxxxxxx,                       xxxxxxx,        xxxxxxx,                      xxxxxxx, xxxxxxx, xxxxxxx
// '--------'        '--------+-----------------+--------'             '-----------------------+--------'                    '--------+--------+--------'
),
*/
};

/**************** Key definition ****************/

// state of the encoder
static enum encoder_mode encoder_cur_mode = VOLUME;
static uint16_t encoder_idle_timer = 0;

// a boolean is needed in addition of the current layer because some action are not on the based layer
static bool isMac = true;

// leader key
LEADER_EXTERNS();

void notify_ok(void);
void notify_ko(void);
void flash_leds(uint8_t hue, uint8_t sat, uint8_t val);

#define SS_FR_C_CEDI      SS_TAP(X_9) // ç
#define SS_FR_E_ACUTE     SS_TAP(X_2) // é
#define SS_FR_E_GRAVE     SS_TAP(X_7) // è
#define SS_FR_A_GRAVE     SS_TAP(X_0) // à
#define SS_FR_GRAVE       SS_ALGR(SS_TAP(X_7)) // `
#define SS_FR_GRAVE_MACOS SS_TAP(X_NUHS) SS_TAP(X_SPACE) // `
#define SS_FR_TILD        SS_ALGR(SS_TAP(X_2)) // ~
#define SS_FR_TILD_MACOS  SS_ALGR(SS_TAP(X_N)) // ~
#define SS_FR_A_CIRC      SS_TAP(X_LBRC) SS_TAP(X_Q) // â
#define SS_FR_E_CIRC      SS_TAP(X_LBRC) SS_TAP(X_E) // ê
#define SS_FR_I_CIRC      SS_TAP(X_LBRC) SS_TAP(X_I) // î
#define SS_FR_O_CIRC      SS_TAP(X_LBRC) SS_TAP(X_O) // ô
#define SS_FR_U_CIRC      SS_TAP(X_LBRC) SS_TAP(X_U) // û
#define SS_FR_U_GRAVE     SS_TAP(X_QUOT) // ù
#define SS_FR_E_TREMA     SS_LSFT(SS_TAP(X_LBRC)) SS_TAP(X_E) // ë
#define SS_FR_I_TREMA     SS_LSFT(SS_TAP(X_LBRC)) SS_TAP(X_I) // ï
#define SS_FR_MICRO       SS_LSFT(SS_TAP(X_NUHS)) // µ
#define SS_FR_MICRO_MACOS SS_LALT(SS_TAP(X_SCLN)) // µ
#define SS_FR_LBRC        SS_ALGR(SS_TAP(X_5)) // [
#define SS_FR_LBRC_MACOS  SS_LSFT(SS_ALGR(SS_TAP(X_5))) // [
#define SS_FR_RBRC        SS_ALGR(SS_TAP(X_MINS)) // ]
#define SS_FR_RBRC_MACOS  SS_LSFT(SS_ALGR(SS_TAP(X_MINS))) // ]
#define SS_FR_EURO        SS_ALGR(SS_TAP(X_E)) // €
#define SS_FR_EURO_MACOS  SS_LSFT(SS_TAP(X_RBRC)) // €
#define SS_FR_EQL_MACOS   SS_TAP(X_SLSH) // =
#define SS_FR_EQL         SS_TAP(X_EQL) // =
#define SS_FR_MINUS_MACOS SS_TAP(X_EQL) // -
#define SS_FR_MINUS       SS_TAP(X_6) // -

void key_double_shift_ss_pc_macos(keyrecord_t *record, bool leading, uint8_t mod_state, const char *str, const char *str_shift_pc, const char *str_shift_macos) {
    if (leading) return;
    if (record->event.pressed) {
        if (mod_state & MOD_MASK_SHIFT) {
            unregister_mods(mod_state);
            wait_ms(DELAY_TAP_CODE);
            if (strlen(str_shift_macos) != 0 && isMac) {
                send_string_with_delay(str_shift_macos, DELAY_TAP_CODE);
            } else {
                send_string_with_delay(str_shift_pc, DELAY_TAP_CODE);
            }
            wait_ms(DELAY_TAP_CODE);
            register_mods(mod_state);
        } else {
            send_string_with_delay(str, DELAY_TAP_CODE);
        }
    }
}

void key_double_shift_ss(keyrecord_t *record, bool leading, uint8_t mod_state, const char *str, const char *str_shift_pc) {
    key_double_shift_ss_pc_macos(record, leading, mod_state, str, str_shift_pc, "");
}

void key_ss_accent(keyrecord_t *record, bool leading, uint8_t mod_state, const char *str) {
    if (leading) return;
    if (record->event.pressed) {
        send_string(str);
        layer_off(_ACCENT);
    }
}
#define KEY_NUMBER_PC_MACOS(NUM, SYM_PC, SYM_MACOS) key_double_shift_ss_pc_macos(record, leading, mod_state, SS_LSFT(SS_TAP(NUM)), SYM_PC, SYM_MACOS); break;
#define KEY_NUMBER(NUM, SYM_SHIFT) key_double_shift_ss_pc_macos(record, leading, mod_state, SS_LSFT(SS_TAP(NUM)), SYM_SHIFT, ""); break;
#define KEY_DOUBLE_SHIFT_SS(SS, SS_SHIFT) key_double_shift_ss_pc_macos(record, leading, mod_state, SS, SS_SHIFT, ""); break;
#define KEY_SS_ACCENT(SS) key_ss_accent(record, leading, mod_state, SS); return false;
#define SS_PC_MACOS(SS_PC, SS_MACOS) if (record->event.pressed) { \
    if (isMac) { \
        SEND_STRING(SS_MACOS); \
    } else { \
        SEND_STRING(SS_PC); \
    } \
    layer_off(_ACCENT); \
    } \
    return false;

// Initialize a boolean variable that keeps track
// of the tab key status: registered or not?
static bool tabkey_registered;

bool process_record_user(uint16_t keycode, keyrecord_t *record) {

    // Store the current modifier state in the variable for later reference
    uint8_t mod_state = get_mods();

// ----------------
// GUI + C, GUI + D and GUI + V are to narrow each other and I press GUI + D too frequently by mistake.
// So, it disables GUI + D if GUI + C has been first press
    static bool copy_registered = false;
    if (keycode == FR_C && record->event.pressed && mod_state & MOD_MASK_GUI) {
         // register GUI + C has been pressed
         copy_registered = true;
    } else if (keycode == FR_D && copy_registered && record->event.pressed && mod_state & MOD_MASK_GUI) {
         notify_ko(); // GUI + D is hit by mistake to often, so disable it
         return false;
    } else if (keycode != KC_LGUI && record->event.pressed) {
         copy_registered = false;
    }

// ----------------
    switch (keycode) {

// ----------------
// number row
    case NUM_1:   KEY_NUMBER_PC_MACOS(X_1, SS_TAP(X_SLSH), SS_TAP(X_8)) // 1 !
    case NUM_2:   KEY_NUMBER_PC_MACOS(X_2, SS_ALGR(SS_TAP(X_0)), SS_TAP(X_NUBS)) // 2 @
    case NUM_3:   KEY_NUMBER_PC_MACOS(X_3, SS_ALGR(SS_TAP(X_3)), SS_LSFT(SS_TAP(X_NUBS))) // 3 #
    case NUM_4:   KEY_NUMBER         (X_4, SS_TAP(X_RBRC)) // 4 $
    case NUM_5:   KEY_NUMBER         (X_5, SS_LSFT(SS_TAP(X_QUOT))) // 5 %
    case NUM_6:   KEY_NUMBER         (X_6, SS_TAP(X_LBRC) SS_DELAY(DELAY_TAP_CODE) SS_TAP(X_SPACE)) // 6 ^
    case NUM_7:   KEY_NUMBER         (X_7, SS_TAP(X_1)) // 7 &
    case NUM_8:   KEY_NUMBER_PC_MACOS(X_8, SS_TAP(X_NUHS), SS_LSFT(SS_TAP(X_RBRC))) // 8 *
    case NUM_9:   KEY_NUMBER         (X_9, SS_TAP(X_5)) // 9 (
    case NUM_0:   KEY_NUMBER         (X_0, SS_TAP(X_MINS)) // 0 )

// ----------------
// punctuation
    case QUOT:    KEY_DOUBLE_SHIFT_SS(SS_TAP(X_4), SS_TAP(X_3)) // ' "
    case SCLN:    KEY_DOUBLE_SHIFT_SS(SS_TAP(X_DOT), SS_TAP(X_COMM)) // : ;
    case COMMA:   key_double_shift_ss_pc_macos(record, leading, mod_state, SS_TAP(X_M), SS_TAP(X_NUBS), SS_TAP(X_GRV)); break; // , <
    case DOT:     key_double_shift_ss_pc_macos(record, leading, mod_state, SS_LSFT(SS_TAP(X_COMM)), SS_LSFT(SS_TAP(X_NUBS)), SS_LSFT(SS_TAP(X_GRV))); break; // . >
    case TILDE:   SS_PC_MACOS(SS_FR_TILD, SS_FR_TILD_MACOS SS_TAP(X_SPACE)) // ~
    case LBRC:    // [
        if (record->event.pressed) {
            if (isMac) {
                SEND_STRING(SS_FR_LBRC_MACOS);
            } else {
                SEND_STRING(SS_FR_LBRC);
            }
        }
        return false;
    case RBRC:    // ]
        if (record->event.pressed) {
            if (isMac) {
                SEND_STRING(SS_FR_RBRC_MACOS);
            } else {
                SEND_STRING(SS_FR_RBRC);
            }
        }
        return false;
    case EURO:    SS_PC_MACOS(SS_FR_EURO, SS_FR_EURO_MACOS) // €
    case GRA_ESC:
        if (record->event.pressed) {
            if (!isMac) {
                if (mod_state & MOD_MASK_CTRL) {
                    tap_code(KC_N); // windows only = ctrl + N
                } else {
                    key_double_shift_ss(record, leading, mod_state, SS_TAP(X_ESC), SS_FR_GRAVE);
                }
            } else {
                key_double_shift_ss(record, leading, mod_state, SS_TAP(X_ESC), SS_FR_GRAVE_MACOS);
            }
        }
        break;
    case LR_FUNC:
        if (record->tap.count > 0) {
            // | backslash
            if (isMac) {
                key_double_shift_ss(record, leading, mod_state, SS_LSFT(SS_ALGR(SS_TAP(X_L))), SS_LSFT(SS_ALGR(SS_TAP(X_DOT))));
            } else {
                key_double_shift_ss(record, leading, mod_state, SS_ALGR(SS_TAP(X_6)), SS_ALGR(SS_TAP(X_8)));
            }
            // do not continue with default tap action
            // if the MT was pressed or released, but not held
            return false;
        }
        break;
    case SLASH:
        if (record->event.pressed) {
            if (mod_state & MOD_MASK_SHIFT) {
                tap_code(FR_COMM); // ?
            } else if (mod_state & MOD_MASK_GUI) {
                tap_code16(FR_COLN); // override : cmd + / instead of cmd + :
            } else {
                SEND_STRING_DELAY(SS_LSFT(SS_TAP(X_DOT)), DELAY_TAP_CODE); // /
            }
        }
        break;
    case MINS:
        {
            static bool mins_underscore = false;
            static uint8_t previous_mod_state = 0;
            if (record->event.pressed) {
                if (mod_state & MOD_MASK_SHIFT) {
                    if (isMac) {
                        register_code(KC_EQL); // _
                    } else {
                        previous_mod_state = mod_state;
                        unregister_mods(MOD_MASK_SHIFT);
                        wait_ms(DELAY_TAP_CODE);
                        register_code(FR_UNDS); // _
                    }
                    mins_underscore = true;
                } else if (isMac && mod_state & MOD_MASK_GUI) {
                    register_code(FR_EQL); // override : cmd + - instead of cmd + 6
                } else {
                  if (isMac) {
                      register_code(KC_EQL); // §
                  } else {
                      register_code(FR_MINS); // -
                  }
                }
            } else {
                if (mins_underscore) {
                    if (isMac) {
                        unregister_code(KC_EQL); // _
                    } else {
                        unregister_code(FR_UNDS); // _
                        register_mods(previous_mod_state);
                        wait_ms(DELAY_TAP_CODE);
                    }
                    mins_underscore = false;
                } else if (isMac && mod_state & MOD_MASK_GUI) {
                    unregister_code(FR_EQL);
                } else {
                  if (isMac) {
                      unregister_code(KC_EQL); // §
                  } else {
                      unregister_code(FR_MINS); // -
                  }
                }
            }
        }
        break;
    case EQUAL:
        if (leading) break;
        if (record->event.pressed) {
            if (isMac && mod_state & MOD_MASK_GUI) {
                tap_code16(KC_SLSH); // override : cmd + + instead of cmd + =
            }  else {
                // same key for PC or mac os for = +
                if (isMac) {
                    SEND_STRING(SS_FR_EQL_MACOS); // = +
                } else {
                    SEND_STRING(SS_FR_EQL); // = +
                }
            }
        }
        break;

// ----------------
// accent
    case E_ACUTE: KEY_SS_ACCENT(SS_FR_E_ACUTE)
    case E_GRAVE: KEY_SS_ACCENT(SS_FR_E_GRAVE)
    case A_GRAVE: KEY_SS_ACCENT(SS_FR_A_GRAVE)
    case A_CIRC:  KEY_SS_ACCENT(SS_FR_A_CIRC)
    case E_CIRC:  KEY_SS_ACCENT(SS_FR_E_CIRC)
    case I_CIRC:  KEY_SS_ACCENT(SS_FR_I_CIRC)
    case O_CIRC:  KEY_SS_ACCENT(SS_FR_O_CIRC)
    case U_CIRC:  KEY_SS_ACCENT(SS_FR_U_CIRC)
    case E_TREMA: KEY_SS_ACCENT(SS_FR_E_TREMA)
    case I_TREMA: KEY_SS_ACCENT(SS_FR_I_TREMA)
    case C_CEDI:  KEY_SS_ACCENT(SS_FR_C_CEDI)
    case MICRO:   SS_PC_MACOS(SS_FR_MICRO, SS_FR_MICRO_MACOS)

// ----------------
// utils
    case MAKE:
        if (record->event.pressed) {
            #if defined(RGBLIGHT_ENABLE)
            rgblight_sethsv_range(HSV_RED, 0, RGBLED_NUM);
            #endif
            if (!(mod_state & MOD_MASK_CTRL)) {
                unregister_mods(mod_state);
                SEND_STRING_DELAY(SS_LCTL(SS_TAP(X_C)) "qmk flash ", DELAY_TAP_CODE);
                send_string_with_delay(isMac ? SS_FR_MINUS_MACOS : SS_FR_MINUS, DELAY_TAP_CODE);
                SEND_STRING_DELAY("kb " QMK_KEYBOARD " ", DELAY_TAP_CODE);
                send_string_with_delay(isMac ? SS_FR_MINUS_MACOS : SS_FR_MINUS, DELAY_TAP_CODE);
                SEND_STRING_DELAY("km " QMK_KEYMAP SS_TAP(X_ENTER), DELAY_TAP_CODE);
            }
            reset_keyboard();
        }
        break;
    case KC_LCTL:
        if (!record->event.pressed && tabkey_registered) {
            tabkey_registered = false;
            unregister_mods(MOD_BIT(KC_LALT));
            return false;
        }
        break;
    case ALT_TAB:
        {
        // for PC: map alt+tab on ctrl+tab
        if (record->event.pressed) {
            // Detect the activation of either ctrl keys
            if (mod_state & MOD_MASK_CTRL) {
                // First temporarily canceling both ctrl so that
                // alt isn't applied to the KC_TAB keycode
                unregister_mods(MOD_MASK_CTRL);
                wait_ms(DELAY_TAP_CODE);
                register_mods(MOD_BIT(KC_LALT));
                // Update the boolean variable to reflect the status of KC_TAB
                tabkey_registered = true;
            }
            register_code(KC_TAB);
        } else {
            unregister_code(KC_TAB);
        }
        }
        break;
    case VERSION:
        if (record->event.pressed) {
            SEND_STRING_DELAY(QMK_KEYBOARD "/" QMK_KEYMAP " , macos:", DELAY_TAP_CODE);
            send_string(isMac ? "1" : "0");
        }
        break;
    case INVALID: // xxxxxxx
        if (record->event.pressed) {
            notify_ko();
        }
        break;
    case KC_BSPC:
        {
        // Initialize a boolean variable that keeps track
        // of the delete key status: registered or not?
        static bool delkey_registered;
        if (record->event.pressed) {
            // Detect the activation of either shift keys
            if (mod_state & MOD_MASK_SHIFT) {
                // First temporarily canceling both shifts so that
                // shift isn't applied to the KC_DEL keycode
                unregister_mods(MOD_MASK_SHIFT);
                wait_ms(DELAY_TAP_CODE);
                register_code(KC_DEL);
                // Update the boolean variable to reflect the status of KC_DEL
                delkey_registered = true;
                // Reapplying modifier state so that the held shift key(s)
                // still work even after having tapped the Backspace/Delete key.
                set_mods(mod_state);
                return false;
            }
        } else { // on release of KC_BSPC
            // In case KC_DEL is still being sent even after the release of KC_BSPC
            if (delkey_registered) {
                unregister_code(KC_DEL);
                delkey_registered = false;
                return false;
            }
        }
        }
        break;
    case ENC_SW:
        if (record->event.pressed) {
            encoder_cur_mode = SCROLL;
            encoder_idle_timer = timer_read();
            return false;
        }
        break;
    }
    // Let QMK process the keycode as usual
    return true;
};

/**************** FUNCTION FOR ADVANCED TAP DANCE ****************/
// https://github.com/qmk/qmk_firmware/blob/master/users/gordon/gordon.c and https://docs.qmk.fm/#/feature_tap_dance?id=example-4-39quad-function-tap-dance39
typedef struct {
    bool is_press_action;
    int state;
} xtap;

enum {
    SINGLE_TAP = 1,
    SINGLE_HOLD = 2,
    DOUBLE_TAP = 3,
    DOUBLE_HOLD = 4,
    DOUBLE_SINGLE_TAP = 5, //send two single taps
    TRIPLE_TAP = 6,
    TRIPLE_HOLD = 7
};

//This works well if you want this key to work as a "fast modifier". It favors being held over being tapped.
int hold_cur_dance (qk_tap_dance_state_t *state) {
    if (state->count == 1) {
        if (state->interrupted) {
            if (!state->pressed) return SINGLE_TAP;
            else return SINGLE_HOLD;
        }
        else {
            if (!state->pressed) return SINGLE_TAP;
            else return SINGLE_HOLD;
        }
    }
    //If count = 2, and it has been interrupted - assume that user is trying to type the letter associated
    //with single tap.
    else if (state->count == 2) {
        if (state->pressed) return DOUBLE_HOLD;
        else return DOUBLE_TAP;
    }
    else if (state->count == 3) {
        if (!state->pressed) return TRIPLE_TAP;
        else return TRIPLE_HOLD;
    }
    else return 8; //magic number. At some point this method will expand to work for more presses
}

void dance_home(qk_tap_dance_state_t *state, void *user_data) {
    if (state->count == 1) {
        if (isMac) {
            SEND_STRING(SS_LGUI(SS_TAP(X_RIGHT)));
        } else {
            tap_code(KC_END);
        }
    } else if (state->count == 2) {
        if (isMac) {
            SEND_STRING(SS_LGUI(SS_TAP(X_LEFT)));
        } else {
            tap_code(KC_HOME);
        }
    }
    reset_tap_dance(state);
}

void dance_smile_happy(qk_tap_dance_state_t *state, void *user_data) {
    if (state->count == 1) {
        SEND_STRING_DELAY(":", DELAY_TAP_CODE);
        send_string_with_delay(isMac ? SS_FR_MINUS_MACOS : SS_FR_MINUS, DELAY_TAP_CODE);
        SEND_STRING(") ");
    } else if (state->count == 2) {
        SEND_STRING_DELAY(";", DELAY_TAP_CODE);
        send_string_with_delay(isMac ? SS_FR_MINUS_MACOS : SS_FR_MINUS, DELAY_TAP_CODE);
        SEND_STRING_DELAY(") ", DELAY_TAP_CODE);
    } else if (state->count >= 3) {
        SEND_STRING_DELAY(":muscle: ", DELAY_TAP_CODE);
    }
    reset_tap_dance(state);
}

void dance_smile_cry(qk_tap_dance_state_t *state, void *user_data) {
    if (state->count == 1) {
        SEND_STRING_DELAY(":", DELAY_TAP_CODE);
        send_string_with_delay(isMac ? SS_FR_MINUS_MACOS : SS_FR_MINUS, DELAY_TAP_CODE);
        SEND_STRING_DELAY("( ", DELAY_TAP_CODE);
    } else if (state->count == 2) {
        SEND_STRING_DELAY(":sob: ", DELAY_TAP_CODE);
    } else if (state->count >=3) {
        SEND_STRING_DELAY(":scream: ", DELAY_TAP_CODE);
    }
    reset_tap_dance(state);
}

// {, SHIFT, SHIFT SHIFT
static xtap shift_state = {
    .is_press_action = true,
    .state = 0
};

void lshift_start(qk_tap_dance_state_t *state, void *user_data) {
    shift_state.state = hold_cur_dance(state);
    switch (shift_state.state) {
        case SINGLE_TAP:  // send { on single press
            if (isMac) {
                SEND_STRING(SS_LALT(SS_TAP(X_5)));
            } else {
                SEND_STRING(SS_RALT(SS_TAP(X_4)));
            }
            break;
        case SINGLE_HOLD: register_code(KC_RSFT); break; // register shift
        case DOUBLE_TAP: register_code(KC_RSFT); wait_ms(DELAY_TAP_CODE); unregister_code(KC_RSFT); wait_ms(DELAY_TAP_CODE); register_code(KC_RSFT); break; // shift shift
    }
}

void lshift_end(qk_tap_dance_state_t *state, void *user_data) {
    switch (shift_state.state) {
        case SINGLE_TAP: break;
        case SINGLE_HOLD: unregister_code (KC_RSFT); break; // unregister shift
        case DOUBLE_TAP: unregister_code(KC_RSFT); break; // shift shift
    }
    shift_state.state = 0;
}

void rshift_start(qk_tap_dance_state_t *state, void *user_data) {
    shift_state.state = hold_cur_dance(state);
    switch (shift_state.state) {
        case SINGLE_TAP: // send } on single press
            if (isMac) {
                SEND_STRING(SS_LALT(SS_TAP(X_MINS)));
            } else {
                SEND_STRING(SS_RALT(SS_TAP(X_EQL)));
            }
            break;
        case SINGLE_HOLD: register_code(KC_RSFT); break; // register shift
        case DOUBLE_TAP: register_code(KC_RSFT); wait_ms(DELAY_TAP_CODE); unregister_code(KC_RSFT); wait_ms(DELAY_TAP_CODE); register_code(KC_RSFT); break; // shift shift
    }
}

void rshift_end(qk_tap_dance_state_t *state, void *user_data) {
    switch (shift_state.state) {
        case SINGLE_TAP: break;
        case SINGLE_HOLD: unregister_code (KC_RSFT); break; // unregister shift
        case DOUBLE_TAP: unregister_code(KC_RSFT); break; // shift shift
    }
    shift_state.state = 0;
}

// Tap Dance definitions
qk_tap_dance_action_t tap_dance_actions[] = {
    [HOME_COMBO] = ACTION_TAP_DANCE_FN(dance_home),
    [SMILEY_HAPPY] = ACTION_TAP_DANCE_FN(dance_smile_happy),
    [SMILEY_CRY] = ACTION_TAP_DANCE_FN(dance_smile_cry),
    [LSHIFT_COMBO] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, lshift_start, lshift_end),
    [RSHIFT_COMBO] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, rshift_start, rshift_end),
};

/**************** RGB logic ****************/

void notify_ok(void) {
    #if defined(RGBLIGHT_ENABLE)
    flash_leds(HSV_SPRINGGREEN);
    #endif
}

void notify_ko(void) {
    #if defined(RGBLIGHT_ENABLE)
    flash_leds(HSV_RED);
    #endif
}

#if defined(RGBLIGHT_ENABLE)
    // led range                 start, end
    #define LED_CONFIRMATION_LEFT    4,  7
    #define LED_CONFIRMATION_RIGHT   11, 14

    void flash_leds(uint8_t hue, uint8_t sat, uint8_t val) {
        rgblight_sethsv_range(hue, sat, val, LED_CONFIRMATION_LEFT);
        rgblight_sethsv_range(hue, sat, val, LED_CONFIRMATION_RIGHT);
        wait_ms(100);
        rgblight_sethsv_range(HSV_OFF, LED_CONFIRMATION_LEFT);
        rgblight_sethsv_range(HSV_OFF, LED_CONFIRMATION_RIGHT);
        wait_ms(100);
        rgblight_sethsv_range(hue, sat, val, LED_CONFIRMATION_LEFT);
        rgblight_sethsv_range(hue, sat, val, LED_CONFIRMATION_RIGHT);
        wait_ms(100);
    }

    // rgblight_blink_layer() behave strangly in 0.12 vs 0.11 :
    // if the dip switch is moved to quickly, the colors led gets stuck
    void light_leds(uint8_t hue, uint8_t sat, uint8_t val) {
        rgblight_sethsv_range(hue, sat, rgblight_get_val() + 50, 0, RGBLED_NUM);
        wait_ms(200);
    }

    void rgblight_set_hsv_and_mode(uint8_t hue, uint8_t sat, uint8_t val, uint8_t mode) {
        rgblight_mode_noeeprom(mode);
        rgblight_sethsv_noeeprom(hue, sat, rgblight_get_val());
    }

    void keyboard_post_init_rgb_light(void) {
        // enables Rgb, without saving settings
        rgblight_enable_noeeprom();
        rgblight_set_speed_noeeprom(RGBLIGHT_DEFAULT_SPD);
        rgblight_mode_noeeprom(RGBLIGHT_DEFAULT_MODE);
        rgblight_sethsv_noeeprom(RGBLIGHT_DEFAULT_HUE, RGBLIGHT_DEFAULT_SAT, RGBLIGHT_DEFAULT_VAL);
    }

#endif

layer_state_t layer_state_set_user(layer_state_t state) {
    switch (get_highest_layer(state)) {
        case _FUNC:
            encoder_cur_mode = SCREEN_LIGHT;
#if defined(RGBLIGHT_ANIMATIONS)
            rgblight_set_hsv_and_mode(HSV_MAGENTA, RGBLIGHT_MODE_STATIC_LIGHT);
#endif
            break;
        default:
            encoder_cur_mode = VOLUME;
#if defined(RGBLIGHT_ANIMATIONS)
            rgblight_mode_noeeprom(RGBLIGHT_DEFAULT_MODE);
#endif
            break;
    }
    return state;
}


/**************** Dip switch ****************/

bool dip_switch_update_user(uint8_t index, bool active) {
    // position, starting closest to usb-c connector to farthest
    // 1st : dip n 0 : 0
    // 2nd : dip n 0 : 1 / dip n 1 : 0
    // 3rd : dip n 1 : 1 / dip n 0 : 0

    // debug
    //SEND_STRING("index: ");
    //char characterToSend[2];
    //characterToSend[0] = '0' + index;
    //characterToSend[1] = '\0';
    //send_string(characterToSend);
    //SEND_STRING(", active: ");
    //send_string(active ? "1" : "0");

    switch (index) {
        case 0:
            if (!active) {
                // 1st position
                layer_off(_PC);
                isMac = true;
                #if defined(RGBLIGHT_ENABLE)
                light_leds(HSV_PINK);
                #endif
            } else {
                // 2nd position
                layer_on(_PC);
                isMac = false;
                #if defined(RGBLIGHT_ENABLE)
                light_leds(HSV_GREEN);
                #endif
            }
            break;
    }
    // debug
    //SEND_STRING(" colmak:");
    //send_string(layer_state_is(_MACOS) ? "1" : "0");
    //SEND_STRING(" pc:");
    //send_string(layer_state_is(_PC) ? "1" : "0");
    //SEND_STRING(SS_TAP(X_ENTER));
    return true;
}

/**************** Encoder ****************/

bool encoder_update_user(uint8_t index, bool clockwise) {
    encoder_idle_timer = timer_read();
    switch (encoder_cur_mode) {
        case SCROLL:
            tap_code(clockwise ? KC_MS_WH_DOWN : KC_MS_WH_UP);
            break;

        case VOLUME:
            tap_code(clockwise ? KC_AUDIO_VOL_UP : KC_AUDIO_VOL_DOWN);
            break;

        case SCREEN_LIGHT:
            tap_code(clockwise ? KC_BRIGHTNESS_UP : KC_BRIGHTNESS_DOWN);
            break;

        default:
            break;
    }
    return false;
}

/**************** Endless loop + leader key ****************/

bool did_leader_succeed;

void matrix_scan_user(void) {

    if (encoder_cur_mode == SCROLL && timer_elapsed(encoder_idle_timer) > 5000) {
        encoder_cur_mode = VOLUME;
    }

    LEADER_DICTIONARY() {
        did_leader_succeed = leading = false;

        SEQ_ONE_KEY(FR_B) {
            SEND_STRING_DELAY("Bonne journ", DELAY_TAP_CODE);
            tap_code16(FR_EACU);
            SEND_STRING("e");
            did_leader_succeed = true;
        }
        SEQ_ONE_KEY(FR_M) {
            SEND_STRING_DELAY("Merci d'avance et bonne journ", DELAY_TAP_CODE);
            tap_code16(FR_EACU);
            SEND_STRING("e");
            did_leader_succeed = true;
        }
        SEQ_ONE_KEY(GRA_ESC) {
            // `|`
            if (IS_LAYER_ON(_PC)) {
                SEND_STRING_DELAY(SS_FR_GRAVE SS_FR_GRAVE SS_TAP(X_LEFT), DELAY_TAP_CODE);
            } else {
                SEND_STRING(SS_FR_GRAVE_MACOS SS_FR_GRAVE_MACOS SS_TAP(X_LEFT));
            }
            did_leader_succeed = true;
        }
        SEQ_TWO_KEYS(GRA_ESC, GRA_ESC) {
            // ```|<enter>```
            if (IS_LAYER_ON(_PC)) {
                SEND_STRING_DELAY(SS_FR_GRAVE SS_FR_GRAVE SS_FR_GRAVE SS_TAP(X_ENT) SS_TAP(X_ENT) SS_FR_GRAVE SS_FR_GRAVE SS_FR_GRAVE, DELAY_TAP_CODE);
            } else {
                SEND_STRING(SS_FR_GRAVE_MACOS SS_FR_GRAVE_MACOS SS_FR_GRAVE_MACOS SS_TAP(X_ENT) SS_TAP(X_ENT) SS_FR_GRAVE_MACOS SS_FR_GRAVE_MACOS SS_FR_GRAVE_MACOS);
            }
            SEND_STRING(SS_TAP(X_LEFT) SS_TAP(X_LEFT) SS_TAP(X_LEFT) SS_TAP(X_LEFT));
            did_leader_succeed = true;
        }
        SEQ_ONE_KEY(NUM_7) {
            // Intelli J > Find usages
            tap_code16(LALT(KC_F7));
            did_leader_succeed = true;
        }
        SEQ_ONE_KEY(NUM_9) {
            // Intelli J > build project
            tap_code16(LGUI(KC_F9));
            did_leader_succeed = true;
        }
        SEQ_ONE_KEY(NUM_0) {
            // Intelli J > build current module
            tap_code16(LGUI(KC_F11));
            did_leader_succeed = true;
        }
        SEQ_ONE_KEY(EQUAL) {
            // Intelli J > file structure
            tap_code16(LGUI(KC_F12));
            did_leader_succeed = true;
        }
        SEQ_ONE_KEY(KC_SPC) {
            // mac os > display emoji
            SEND_STRING(SS_DOWN(X_LCTL) SS_DOWN(X_LGUI) SS_TAP(X_SPC) SS_UP(X_LGUI) SS_UP(X_LCTL));
            did_leader_succeed = true;
        }

        #if (__has_include("secrets.c") && !defined(NO_SECRETS))
        #include "secrets.c"
        #endif
        leader_end();
    }
}

void leader_end(void) {
    if (did_leader_succeed) {
        notify_ok();
    } else {
        notify_ko();
    }
}

void keyboard_post_init_user(void) {
    #if defined(RGBLIGHT_ENABLE)
    keyboard_post_init_rgb_light();
    #endif
}
