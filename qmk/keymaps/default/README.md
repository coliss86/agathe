# Default Keymap

This is a Qwerty layout with dedicated accent keys that send accents as OS keycodes.

> Note: the keyboard **must** be recognized as 'QWERTY' on the OS side, and the corresponding layer must be activated on the keyboard depending on the OS (`_QWERTY` is activated by default for MacOS, `_PC` for Linux or Windows).

## Features 🔮

Special features of these layouts:
- [Space cadet shift](https://docs.qmk.fm/#/feature_space_cadet) (Orange Shift Key): tap shift: `{`, hold: `shift`
- [Leader key](https://docs.qmk.fm/#/feature_leader_key) (Purple Leader Key): bunch of shortcuts
- [Tap dance](https://docs.qmk.fm/#/feature_tap_dance): one tap: `:-)`, two taps: `;-)`, three taps: `:muscle:`...
- LED flash when a wrong combo is typed or when a mapping on a layer is `xxxxxxx`: see the [`flash_leds() function` in keymap.c](keymap.c)

## Layouts 👗👔

Qwerty:

![Qwerty Layout](doc/layout_default-base.png)

French accents: çîïëùûô, euro € :

![Accent Layout](doc/layout_default-accent.png)

Function layout: F1-F10, player play/pause, player next, Atmega Reset, volume +/-, screen brightness +/- :

![Func Layout](doc/layout_func.png)

RGB Control: next effect, increase / decrease brightness, change effet and color, toggle on / off :

![RGB Layout](doc/layout_rgb.png)

## Compile and flash

### Keyboard shortcut

Press: <kbd>|</kbd> + <kbd>Click on rotary</kbd>.

See the definition of `RESET` in the `_FUNC` layer and its usage in [the function `process_record_user()` in keymap.c](keymap.c).

### CLI

```
qmk flash -kb agathe -km colemak
```
and press the reset button on the back of the keyboard, located to the left of the USB-C connector on the PCB. See the picture below.

<img src="doc/reset_button.jpg" alt="Reset button" width="400"/>
