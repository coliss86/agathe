/*
Copyright 2021 Coliss86

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include QMK_KEYBOARD_H

#include "version.h"

#define RGB_LAYER_ACK_DURATION 500
#define TAPPING_TERM_BRACE 150

bool onMac = true;

enum layers {
    _QWERTY = 0,
    _PC,
    _FUNC,
    _ACCENT,
    _RGB,
};

// Macro Declarations
enum custom_keycodes {
    A_GRAVE = SAFE_RANGE,
    E_ACUTE,
    E_GRAVE,
    E_CIRC,
    E_TREMA,
    U_GRAVE,
    U_CIRC,
    I_CIRC,
    I_TREMA,
    O_CIRC,
    C_CEDI,
    EURO,
    MICRO,
    M_RESET,
    M_VERS,
    INVALID,
    RSFT_BR,
    LSFT_BR,
    ACCENT,
    DEGREE,
    PIPE,
};

// Tap Dance declarations
enum tap_dance {
    TAP_DANCE_HOME,
    TAP_DANCE_SMILE_HAPPY,
    TAP_DANCE_SMILE_CRY,
    TAP_DANCE_GRAVE,
};

#define LR_FUNC     LT(_FUNC, KC_SPC)
#define LR_RGB      LT(_RGB, KC_TAB)
#define TD_GRV      TD(TAP_DANCE_GRAVE)
#define TD_HAPP     TD(TAP_DANCE_SMILE_HAPPY)
#define TD_CRY      TD(TAP_DANCE_SMILE_CRY)
#define TD_HOME     TD(TAP_DANCE_HOME)
#define xxxxxxx     INVALID

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

[_QWERTY] = LAYOUT(
// ,--------+--------+--------+--------+--------+--------+--------.    |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    KC_GESC, KC_1,    KC_2,    TD_GRV,  KC_4,    KC_5,    KC_6,         KC_MINS, KC_7,    KC_8,    KC_9,    KC_0,    KC_EQL,  KC_BSPC,          TD_CRY,
// |--------+--------+--------+--------+--------+--------+--------'    |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    LR_RGB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,                  A_GRAVE, KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_QUOT, PIPE,    TD_HAPP,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    KC_BSPC, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,                  E_ACUTE, KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_ENT,           TD_HOME,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    LSFT_BR, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,                  ACCENT,  KC_SLSH, KC_N,    KC_M,    KC_COMM, KC_DOT,  RSFT_BR, KC_UP,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    KC_LCTL,          KC_LGUI, LR_FUNC,          KC_LALT,               KC_SPC,                 KC_RGUI,                      KC_LEFT, KC_DOWN, KC_RGHT
// '--------'        '--------+-----------------+--------'             '-----------------------+--------'                    '--------+--------+--------'
),

[_PC] = LAYOUT(
// ,--------+--------+--------+--------+--------+--------+--------.    |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    _______, _______, _______, _______, _______, _______, _______,      _______, _______, _______, _______, _______, _______, _______,          _______,
// |--------+--------+--------+--------+--------+--------+--------'    |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______, _______, _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______,          _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______, _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______,          KC_LALT, _______,          KC_LGUI,               _______,                _______,                      _______, _______, _______
// '--------'        '--------+-----------------+--------'             '-----------------------+--------'                    '--------+--------+--------'
),

[_FUNC] = LAYOUT(
// ,--------+--------+--------+--------+--------+--------+--------.    |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    xxxxxxx, KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,        xxxxxxx, KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_DEL,           M_RESET,
// |--------+--------+--------+--------+--------+--------+--------'    |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx, xxxxxxx, KC_VOLU, KC_MPLY, xxxxxxx, xxxxxxx,               xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, M_VERS,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    KC_DEL,  xxxxxxx, KC_VOLD, KC_MNXT, xxxxxxx, xxxxxxx,               xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,          xxxxxxx,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx, xxxxxxx, KC_MUTE, xxxxxxx, xxxxxxx, xxxxxxx,               xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, KC_PGUP,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx,          xxxxxxx, _______,          xxxxxxx,               xxxxxxx,                xxxxxxx,                      KC_HOME, KC_PGDN, KC_END
// '--------'        '--------+-----------------+--------'             '-----------------------+--------'                    '--------+--------+--------'
),

[_ACCENT] = LAYOUT(
// ,--------+--------+--------+--------+--------+--------+--------.    |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, EURO,    xxxxxxx, xxxxxxx,      xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, DEGREE,  xxxxxxx, xxxxxxx,          xxxxxxx,
// |--------+--------+--------+--------+--------+--------+--------'    |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx, xxxxxxx, xxxxxxx, E_CIRC,  xxxxxxx, xxxxxxx,               xxxxxxx, U_CIRC,  U_GRAVE, I_TREMA, O_CIRC,  xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx, A_GRAVE, xxxxxxx, E_TREMA, xxxxxxx, xxxxxxx,               xxxxxxx, xxxxxxx, xxxxxxx, I_CIRC,  xxxxxxx, xxxxxxx, xxxxxxx,          xxxxxxx,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx, xxxxxxx, xxxxxxx, C_CEDI , xxxxxxx, xxxxxxx,               xxxxxxx, xxxxxxx, KC_TILD, MICRO,   xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx,          xxxxxxx, xxxxxxx,          xxxxxxx,               xxxxxxx,                xxxxxxx,                      xxxxxxx, xxxxxxx, xxxxxxx
// '--------'        '--------+-----------------+--------'             '-----------------------+--------'                    '--------+--------+--------'
),

[_RGB] = LAYOUT(
// ,--------+--------+--------+--------+--------+--------+--------.    |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,      xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,          xxxxxxx,
// |--------+--------+--------+--------+--------+--------+--------'    |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, RGB_MOD, RGB_TOG,               RGB_M_P, RGB_M_B, RGB_M_R, RGB_M_SW,xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx, xxxxxxx, RGB_SPD, RGB_HUD, RGB_VAD, RGB_SAD,               RGB_M_SN,RGB_M_K, RGB_M_X, RGB_M_G, xxxxxxx, xxxxxxx, xxxxxxx,          xxxxxxx,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx, xxxxxxx, RGB_SPI, RGB_HUI, RGB_VAI, RGB_SAI,               xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx,          xxxxxxx, xxxxxxx,          xxxxxxx,               xxxxxxx,                xxxxxxx,                      xxxxxxx, xxxxxxx, xxxxxxx
// '--------'        '--------+-----------------+--------'             '-----------------------+--------'                    '--------+--------+--------'
),

/*
[NAME] = LAYOUT(
// ,--------+--------+--------+--------+--------+--------+--------.    |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    _______, _______, _______, _______, _______, _______, _______,      _______, _______, _______, _______, _______, _______, _______,          _______,
// |--------+--------+--------+--------+--------+--------+--------'    |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______, _______, _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______,          _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______, _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______,          _______, _______,          _______,               _______,                _______,                      _______, _______, _______
// '--------'        '--------+-----------------+--------'             '-----------------------+--------'                    '--------+--------+--------'
),
*/
};

/******* FRENCH ACCENT HELPER FUNCTIONS & DECLARATIONS *************/
/* from dztech/dz65rgb/keymaps/drootz/keymap.c */
/*Most comonly used accents only*/

enum french_letter {
    _A,
    _E,
    _I,
    _O,
    _U
};

const uint8_t french_letter_index[5] = {
    [_A] = 0,
    [_E] = 1,
    [_I] = 2,
    [_O] = 3,
    [_U] = 4
};

enum french_accent {
    _CIRCUMFLEX,
    _GRAVE,
    _ACUTE,
    _TREMA
};

const uint8_t french_accent_index[4] = {
    [_CIRCUMFLEX] = 0,
    [_GRAVE] = 1,
    [_ACUTE] = 2,
    [_TREMA] = 3
};

/*
This represent unicode decimal values
Each index will be mapped to numpad keycode to out put the correct sequence
All codes in this array should be of size 3
All accent codes have the same index position as in the french_accent_index Array
*/
const uint8_t french_decimal_unicodes[5][4][2] = { /*[Letter][Accent][Case]*/
    {
        {
            131,    // â
            194     // Â
        },{
            133,    // à
            192     // À
        }
    },{
        {
            136,    // ê
            202     // Ê
        },{
            138,    // è
            200     // È
        },{
            130,    // é
            201     // É
        }, {
            137,    // ë
            203,    // Ë
        }
    },{
        {
            140,    // î
            206     // Î
        }
    },{
        {
            147,    // ô
            212     // Ô
        }
    },{
        {
            150,    // û
            219     // Û
        },{
            151,    // ù
            217     // Ù
        }
    }
};

void break_int_in_array(uint8_t int_code, uint8_t size, uint8_t *array) {
    uint8_t i;
    i = size;
    while (i--) {
        array[i] = int_code%10;
        int_code /= 10;
    }
}

/*
Function meant to be used in Leader Key macros to output most commonly used french accents
This is designed and work on an English language keyboard setting on both Windows and Mac
=> accept french_letter and french_accent enum's as argument
*/
void send_french_accent(uint8_t letter, uint8_t accent) {

    bool isCaps;
    uint8_t decimal_unicode_in;
    uint8_t decimal_unicode_size = 3;
    uint8_t decimal_unicode_out[decimal_unicode_size];

    /*Map to numpad keycodes*/
    const uint16_t numpad_key_map[10] = {
        KC_P0, KC_P1, KC_P2, KC_P3, KC_P4, KC_P5, KC_P6, KC_P7, KC_P8, KC_P9
    };

    /*Map to letter keycodes*/
    const uint16_t french_letter_key_map[5] = {
        KC_A, KC_E, KC_I, KC_O, KC_U
    };

    /*Map to mod keys for French Mac shortcuts*/
    const uint16_t osx_mod_key_map[4] = {
        KC_I, // _CIRCUMFLEX
        KC_GRAVE, // _GRAVE
        KC_E, // _ACUTE
        KC_U // _TREMA
    };

    /*
    Function to tap the correct keycodes in sequence for the
    "Windows Alt Code" requested, aka Decimal Unicodes
    */
    void tap_win_alt_code(void) {
        if (isCaps) {
            tap_code(numpad_key_map[0]); // Leading 0 on all upper case "Windows alt codes"
        }
        for (int i = 0; i < decimal_unicode_size; ++i) {
            tap_code(numpad_key_map[decimal_unicode_out[i]]);
        }
    }

    isCaps = IS_HOST_LED_ON(USB_LED_CAPS_LOCK) ? true : false;

    if (onMac) {
        if (isCaps) {
            SEND_STRING(SS_TAP(X_CAPSLOCK));
            register_code(KC_LALT);
            tap_code(osx_mod_key_map[accent]);
            unregister_code(KC_LALT);
            register_code(KC_LSFT);
            tap_code(french_letter_key_map[letter]);
            unregister_code(KC_LSFT);
            tap_code(KC_CAPS);
        } else {
            register_code(KC_LALT);
            tap_code(osx_mod_key_map[accent]);
            unregister_code(KC_LALT);
            tap_code(french_letter_key_map[letter]);
        }
    } else {
        /*get the correct decimal unicode*/
        decimal_unicode_in = isCaps ? french_decimal_unicodes[letter][accent][1] : french_decimal_unicodes[letter][accent][0];
        break_int_in_array(decimal_unicode_in, decimal_unicode_size, decimal_unicode_out);
        register_code(KC_LALT);
        tap_win_alt_code();
        unregister_code(KC_LALT);
    }
}

// led range                 start, end
#define LED_CONFIRMATION_LEFT    4,  7
#define LED_CONFIRMATION_RIGHT   11, 14

void blink_confirmation(uint8_t hue, uint8_t sat, uint8_t val) {
    rgblight_sethsv_range(hue, sat, val, LED_CONFIRMATION_LEFT);
    rgblight_sethsv_range(hue, sat, val, LED_CONFIRMATION_RIGHT);
    wait_ms(100);
    rgblight_sethsv_range(HSV_OFF, LED_CONFIRMATION_LEFT);
    rgblight_sethsv_range(HSV_OFF, LED_CONFIRMATION_RIGHT);
    wait_ms(50);
    rgblight_sethsv_range(hue, sat, val, LED_CONFIRMATION_LEFT);
    rgblight_sethsv_range(hue, sat, val, LED_CONFIRMATION_RIGHT);
    wait_ms(100);
}

static uint16_t brace_timer;

#define INVERT_SHIFT_KEYCODE(KEYCODE) \
    if (record->event.pressed) { \
        if (mod_state & MOD_MASK_SHIFT) { \
            /* unregister shift, send keycode, register shift */ \
            unregister_mods(mod_state); \
            tap_code16(KEYCODE); \
            register_mods(mod_state); \
        } else { \
            /* send keycode */ \
            tap_code16(S(KEYCODE)); \
        } \
    } \
    break;

#define SPACE_CADET_SHIFT_CTRL(KC_ON_SHIFT, KC_ON_CTRL_SHIFT) \
    if (record->event.pressed) { \
        brace_timer = timer_read(); \
        register_code(KC_LSFT); \
    } else { \
        unregister_code(KC_LSFT); \
        if (timer_elapsed(brace_timer) < TAPPING_TERM_BRACE) { \
            if (mod_state & MOD_MASK_CTRL) { \
                unregister_code(KC_LCTL); \
                tap_code16(KC_ON_SHIFT); \
                register_code(KC_LCTL); \
            } else { \
                tap_code16(KC_ON_CTRL_SHIFT); \
            } \
        } \
    } \
    break;

#define FRENCH_ACCENT(_LET, _ACC) \
    if (record->event.pressed) { \
        send_french_accent(_LET, _ACC); \
    } \
    break;


// Initialize variable holding the binary representation of active modifiers.
uint8_t mod_state;
bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    // Store the current modifier state in the variable for later reference
    mod_state = get_mods();
    switch (keycode) {

// ----------------
// accent keycode
    case A_GRAVE:
        FRENCH_ACCENT(_A, _GRAVE)
    case E_ACUTE:
        FRENCH_ACCENT(_E, _ACUTE);
    case E_GRAVE:
        FRENCH_ACCENT(_E, _GRAVE);
    case E_CIRC:
        FRENCH_ACCENT(_E, _CIRCUMFLEX);
    case E_TREMA:
        FRENCH_ACCENT(_E, _TREMA);
    case U_GRAVE:
        FRENCH_ACCENT(_U, _GRAVE);
    case U_CIRC:
        FRENCH_ACCENT(_U, _CIRCUMFLEX);
    case I_CIRC:
        FRENCH_ACCENT(_I, _CIRCUMFLEX);
    case I_TREMA:
        FRENCH_ACCENT(_I, _TREMA);
    case O_CIRC:
        FRENCH_ACCENT(_O, _CIRCUMFLEX);
    case C_CEDI:
        if (record->event.pressed) {
            if (onMac) {
                //ç ----> alt+c
                SEND_STRING(SS_LALT("c"));
            } else {
                // SEND_STRING(SS_DOWN(X_LALT) SS_TAP(X_P1) SS_TAP(X_P2) SS_TAP(X_P8) SS_UP(X_LALT));
                SEND_STRING(SS_DOWN(X_LALT) SS_TAP(X_P1) SS_TAP(X_P3) SS_TAP(X_P5) SS_UP(X_LALT));
            }
        }
        break;
    case DEGREE:
        if (record->event.pressed) {
            if (onMac) {
              //º ----> alt+0
              SEND_STRING(SS_LALT(SS_TAP(X_0)));
            }
        }
        break;
    case EURO:
        if (record->event.pressed) {
            if (onMac) {
              //€ ----> alt+shift+2
              SEND_STRING(SS_LALT(SS_LSFT(SS_TAP(X_2))));
            }
        }
        break;
    case MICRO:
        if (record->event.pressed) {
            if (onMac) {
              //~ ----> alt+m
              SEND_STRING(SS_LALT(SS_TAP(X_M)));
            }
        }
        break;
    case ACCENT:
        {
        static uint16_t accent_timer;
        // switch to layer _ACCENT on hold, type è on tap
        if (record->event.pressed) {
            accent_timer = timer_read();
            layer_on(_ACCENT);
            return false;
        } else {
            layer_off(_ACCENT);
            if (timer_elapsed(accent_timer) < TAPPING_TERM_BRACE) {
                send_french_accent(_E, _GRAVE);
            }
        }
        }
        break;

// ----------------
// end accent keycode

    case M_RESET:
        if (record->event.pressed) {
            rgblight_sethsv_range(HSV_RED, 0, RGBLED_NUM);
            if (!(mod_state & MOD_MASK_SHIFT)) {
                SEND_STRING(SS_DOWN(X_LCTL) SS_TAP(X_C) SS_UP(X_LCTL) "make " QMK_KEYBOARD ":" QMK_KEYMAP ":flash" SS_TAP(X_ENTER));
            }
            reset_keyboard();
        }
        break;
    case M_VERS:
        if (record->event.pressed) {
            SEND_STRING(QMK_KEYBOARD "/" QMK_KEYMAP " @ " QMK_VERSION ", Built on: " QMK_BUILDDATE ", mac:");
            send_string(onMac ? "1" : "0");
        }
    case INVALID: // xxxxxxx
        if (record->event.pressed) {
            blink_confirmation(HSV_RED);
        }
        break;
    case KC_BSPC:
        {
        // Initialize a boolean variable that keeps track
        // of the delete key status: registered or not?
        static bool delkey_registered;
        if (record->event.pressed) {
            // Detect the activation of either shift keys
            if (mod_state & MOD_MASK_SHIFT) {
                // First temporarily canceling both shifts so that
                // shift isn't applied to the KC_DEL keycode
                del_mods(MOD_MASK_SHIFT);
                register_code(KC_DEL);
                // Update the boolean variable to reflect the status of KC_DEL
                delkey_registered = true;
                // Reapplying modifier state so that the held shift key(s)
                // still work even after having tapped the Backspace/Delete key.
                set_mods(mod_state);
                return false;
            }
        } else { // on release of KC_BSPC
            // In case KC_DEL is still being sent even after the release of KC_BSPC
            if (delkey_registered) {
                unregister_code(KC_DEL);
                delkey_registered = false;
                return false;
            }
        }
        }
        break;
    case PIPE:
        // invert | and \ : pipe by default, SHIFT for backslash
        INVERT_SHIFT_KEYCODE(KC_BSLS)
    case LSFT_BR:
        // shift on hold, [ on tap if CTRL is held, { otherwise
        SPACE_CADET_SHIFT_CTRL(KC_LBRC, KC_LEFT_CURLY_BRACE)
    case RSFT_BR:
        // shift on hold, ] on tap if CTRL is held, } otherwise
        SPACE_CADET_SHIFT_CTRL(KC_RBRC, KC_RIGHT_CURLY_BRACE)
    }
    // Let QMK process the keycode as usual
    return true;
}

void dance_smile_happy(qk_tap_dance_state_t *state, void *user_data) {
    if (state->count == 1) {
        SEND_STRING(":-) ");
    } else if (state->count == 2) {
        SEND_STRING(";-) ");
    } else if (state->count >= 3) {
        SEND_STRING(":muscle: ");
    }
    reset_tap_dance(state);
}

void dance_smile_cry(qk_tap_dance_state_t *state, void *user_data) {
    if (state->count == 1) {
        SEND_STRING(":-( ");
    } else if (state->count >= 2) {
        SEND_STRING(":sob: ");
    }
    reset_tap_dance(state);
}

void dance_grave(qk_tap_dance_state_t *state, void *user_data) {
    if (state->count == 1) {
        SEND_STRING(SS_TAP(X_3));
    } else if (state->count == 2) {
        SEND_STRING(SS_TAP(X_GRAVE));
    } else if (state->count >= 3) {
        // ```|<enter>```
        SEND_STRING(SS_TAP(X_GRAVE) SS_TAP(X_GRAVE) SS_TAP(X_GRAVE) SS_TAP(X_ENT) SS_TAP(X_ENT) SS_TAP(X_GRAVE) SS_TAP(X_GRAVE) SS_TAP(X_GRAVE) SS_TAP(X_LEFT) SS_TAP(X_LEFT) SS_TAP(X_LEFT) SS_TAP(X_LEFT));
    }
    reset_tap_dance(state);
}

// Tap Dance definitions
qk_tap_dance_action_t tap_dance_actions[] = {
    [TAP_DANCE_HOME] = ACTION_TAP_DANCE_DOUBLE(KC_END, KC_HOME),
    [TAP_DANCE_SMILE_HAPPY] = ACTION_TAP_DANCE_FN(dance_smile_happy),
    [TAP_DANCE_SMILE_CRY] = ACTION_TAP_DANCE_FN(dance_smile_cry),
    [TAP_DANCE_GRAVE] = ACTION_TAP_DANCE_FN(dance_grave),
};

/*
 * RGB LED placement
 * .-------------------- USB -------------------------------------.
 * |     14        15         0         1         2         3     |
 * | 13                                                         4 |
 * |                      View from top                           |
 * | 12                                                         5 |
 * |   11                                      7             6    |
 * '----------\  10                 8   /-------------------------'
 *             \----        9      ----/
 *                   \------------/
 */

void keyboard_post_init_user(void) {
    // enables Rgb, without saving settings
    rgblight_enable_noeeprom();
    rgblight_mode_noeeprom(RGBLIGHT_MODE_RAINBOW_SWIRL + 2);
}
