# Azerty Keymap

This is a standard Azerty layout.

### Layouts

Special features of those layouts :
- [Space cadet shift](https://beta.docs.qmk.fm/using-qmk/advanced-keycodes/feature_space_cadet) (Orange Shift Key) : tap on shift : `{`, hold : `shift`
- [Leader key](https://beta.docs.qmk.fm/using-qmk/advanced-keycodes/feature_leader_key) (Orange Leader Key) : bunch of shortcuts
- [Tap dance](https://beta.docs.qmk.fm/using-qmk/software-features/feature_tap_dance) (Yellow key) : one tap : `:-)`, two tap : `;-)`, three tap : `:muscle:`...
- LED flash when a wrong combo is used with `xxxxxxx` : see the [`flash_leds() function` in keymap.c](keymap.c)

Azerty :

![Qwerty Layout](doc/layout_azerty-base.png)

Function layout : F1-F10, player play/pause, player next, Atmega Reset, volume +/-, screen brightness +/- :

![Func Layout](doc/layout_func.png)

RGB Control : next effect, increase / decrease brightness, change effet and color, toggle on / off :

![RGB Layout](doc/layout_rgb.png)

## Compile and flash

### Keyboard shortcut

Press : <kbd>Left SPACE</kbd> + <kbd>Cry smiley</kbd>.

See the definition of `M_RESET` in the `_FUNC` layer and its usage in [the function `process_record_user()` keymap.c](keymap.c).

### CLI

```
qmk flash -kb agathe -km azerty
```
and press the reset button on the back of the keyboard, located to the left of the USB-C connector on the PCB. See picture below.

<img src="doc/reset_button.jpg" alt="Reset button" width="400"/>
