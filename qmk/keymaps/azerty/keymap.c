/*
Copyright 2021 Coliss86

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include QMK_KEYBOARD_H

#include "version.h"
#include "keymap_french.h"
#include "sendstring_french.h"

#define RGB_LAYER_ACK_DURATION 500
#define TAPPING_TERM_BRACE 150

bool onMac = true;

enum layers {
    _AZERTY = 0,
    _PC,
    _FUNC,
    _RGB,
};

// Macro Declarations
enum custom_keycodes {
    M_RESET = SAFE_RANGE,
    M_VERS,
    INVALID,
    GRA_ESC,
};

// Tap Dance declarations
enum tap_dance {
    TAP_DANCE_HOME,
    TAP_DANCE_SMILE_HAPPY,
    TAP_DANCE_SMILE_CRY,
    TAP_DANCE_GRAVE,
};

#define LR_FUNC     LT(_FUNC, KC_SPC)
#define LR_RGB      LT(_RGB, KC_TAB)
#define TD_GRV      TD(TAP_DANCE_GRAVE)
#define TD_HAPP     TD(TAP_DANCE_SMILE_HAPPY)
#define TD_CRY      TD(TAP_DANCE_SMILE_CRY)
#define TD_HOME     TD(TAP_DANCE_HOME)
#define xxxxxxx     INVALID

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

[_AZERTY] = LAYOUT(
// ,--------+--------+--------+--------+--------+--------+--------.    |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    GRA_ESC, FR_AMPR, FR_EACU, FR_DQUO, FR_QUOT, FR_LPRN, FR_MINS,      FR_EGRV, FR_UNDS, FR_CCED, FR_AGRV, FR_RPRN, FR_EQL,  KC_BSPC,          TD_CRY,
// |--------+--------+--------+--------+--------+--------+--------'    |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    LR_RGB,  FR_A,    FR_Z,    FR_E,    FR_R,    FR_T,                  FR_Y,    FR_U,    FR_I,    FR_O,    FR_P,    FR_CIRC, FR_DLR,  FR_ASTR, TD_HAPP,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    KC_BSPC, FR_Q,    FR_S,    FR_D,    FR_F,    FR_G,                  FR_H,    FR_J,    FR_K,    FR_L,    FR_M,    FR_UGRV, KC_ENT,           TD_HOME,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    KC_LSPO, FR_LABK , FR_W,   FR_X,    FR_C,    FR_V,                  FR_B,    FR_N,    FR_COMM, FR_SCLN, FR_COLN, FR_EXLM, KC_RSPC, KC_UP,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    KC_LCTL,          KC_LGUI, LR_FUNC,          KC_LALT,               KC_SPC,                 KC_LEAD,                      KC_LEFT, KC_DOWN, KC_RGHT
// '--------'        '--------+-----------------+--------'             '-----------------------+--------'                    '--------+--------+--------'
),

[_PC] = LAYOUT(
// ,--------+--------+--------+--------+--------+--------+--------.    |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    _______, _______, _______, _______, _______, _______, _______,      _______, _______, _______, _______, _______, _______, _______,          _______,
// |--------+--------+--------+--------+--------+--------+--------'    |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______, _______, _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______,          _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______, _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______,          KC_LALT, _______,          KC_LGUI,               _______,                _______,                      _______, _______, _______
// '--------'        '--------+-----------------+--------'             '-----------------------+--------'                    '--------+--------+--------'
),

[_FUNC] = LAYOUT(
// ,--------+--------+--------+--------+--------+--------+--------.    |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    xxxxxxx, KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,        xxxxxxx, KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_DEL,           M_RESET,
// |--------+--------+--------+--------+--------+--------+--------'    |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx, xxxxxxx, KC_VOLU, KC_MPLY, xxxxxxx, xxxxxxx,               xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, M_VERS,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    KC_DEL,  xxxxxxx, KC_VOLD, KC_MNXT, xxxxxxx, xxxxxxx,               xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,          xxxxxxx,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx, xxxxxxx, KC_MUTE, xxxxxxx, xxxxxxx, xxxxxxx,               xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, KC_PGUP,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx,          xxxxxxx, _______,          xxxxxxx,               xxxxxxx,                xxxxxxx,                      KC_HOME, KC_PGDN, KC_END
// '--------'        '--------+-----------------+--------'             '-----------------------+--------'                    '--------+--------+--------'
),

[_RGB] = LAYOUT(
// ,--------+--------+--------+--------+--------+--------+--------.    |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,      xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,          xxxxxxx,
// |--------+--------+--------+--------+--------+--------+--------'    |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, RGB_MOD, RGB_TOG,               RGB_M_P, RGB_M_B, RGB_M_R, RGB_M_SW,xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx, xxxxxxx, RGB_SPD, RGB_HUD, RGB_VAD, RGB_SAD,               RGB_M_SN,RGB_M_K, RGB_M_X, RGB_M_G, xxxxxxx, xxxxxxx, xxxxxxx,          xxxxxxx,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx, xxxxxxx, RGB_SPI, RGB_HUI, RGB_VAI, RGB_SAI,               xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    xxxxxxx,          xxxxxxx, xxxxxxx,          xxxxxxx,               xxxxxxx,                xxxxxxx,                      xxxxxxx, xxxxxxx, xxxxxxx
// '--------'        '--------+-----------------+--------'             '-----------------------+--------'                    '--------+--------+--------'
),

/*
[NAME] = LAYOUT(
// ,--------+--------+--------+--------+--------+--------+--------.    |--------+--------+--------+--------+--------+--------+--------+--------+--------.
    _______, _______, _______, _______, _______, _______, _______,      _______, _______, _______, _______, _______, _______, _______,          _______,
// |--------+--------+--------+--------+--------+--------+--------'    |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______, _______, _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______,          _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______, _______, _______, _______, _______, _______,               _______, _______, _______, _______, _______, _______, _______, _______,
// |--------+--------+--------+--------+--------+--------|             |--------+--------+--------+--------+--------+--------+--------+--------+--------|
    _______,          _______, _______,          _______,               _______,                _______,                      _______, _______, _______
// '--------'        '--------+-----------------+--------'             '-----------------------+--------'                    '--------+--------+--------'
),
*/
};

// leader key
LEADER_EXTERNS();

#if defined(RGBLIGHT_ENABLE)
// led range                 start, end
#define LED_CONFIRMATION_LEFT    4,  7
#define LED_CONFIRMATION_RIGHT   11, 14

void flash_leds(uint8_t hue, uint8_t sat, uint8_t val) {
    rgblight_sethsv_range(hue, sat, val, LED_CONFIRMATION_LEFT);
    rgblight_sethsv_range(hue, sat, val, LED_CONFIRMATION_RIGHT);
    wait_ms(100);
    rgblight_sethsv_range(HSV_OFF, LED_CONFIRMATION_LEFT);
    rgblight_sethsv_range(HSV_OFF, LED_CONFIRMATION_RIGHT);
    wait_ms(50);
    rgblight_sethsv_range(hue, sat, val, LED_CONFIRMATION_LEFT);
    rgblight_sethsv_range(hue, sat, val, LED_CONFIRMATION_RIGHT);
    wait_ms(100);
}
#endif

void notify_ok(void) {
    #if defined(RGBLIGHT_ENABLE)
    flash_leds(HSV_SPRINGGREEN);
    #endif
}

void notify_ko(void) {
    #if defined(RGBLIGHT_ENABLE)
    flash_leds(HSV_RED);
    #endif
}

#define SS_FR_GRAVE   SS_ALGR(SS_TAP(X_7)) SS_TAP(X_SPACE) // `

bool key_double_shift(keyrecord_t *record, bool leading, uint8_t mod_state, uint16_t kc, uint16_t kc_shift, bool space) {
    if (leading) return true;
    if (record->event.pressed) {
        if (mod_state & MOD_MASK_SHIFT) {
            /* unregister shift, send keycode, register shift */
            unregister_mods(mod_state);
            tap_code16(kc_shift);
            if (space) tap_code(KC_SPACE);
            register_mods(mod_state);
        } else {
            tap_code16(kc);
        }
    }
    return true;
}

#define KEY_DOUBLE_SHIFT_DEAD(KC, KC_SHIFT) return key_double_shift(record, leading, mod_state, KC, KC_SHIFT, true);

// Initialize variable holding the binary representation of active modifiers.
uint8_t mod_state;
bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    // Store the current modifier state in the variable for later reference
    mod_state = get_mods();
    switch (keycode) {

    case GRA_ESC: KEY_DOUBLE_SHIFT_DEAD(KC_ESC, FR_GRV)

    case M_RESET:
        if (record->event.pressed) {
            #if defined(RGBLIGHT_ENABLE)
            rgblight_sethsv_range(HSV_RED, 0, RGBLED_NUM);
            #endif
            if (!(mod_state & MOD_MASK_SHIFT)) {
                SEND_STRING(SS_DOWN(X_LCTL) SS_TAP(X_C) SS_UP(X_LCTL) "qmk flash -kb " QMK_KEYBOARD " -km " QMK_KEYMAP SS_TAP(X_ENTER));
            }
            reset_keyboard();
        }
        break;
    case M_VERS:
        if (record->event.pressed) {
            SEND_STRING(QMK_KEYBOARD "/" QMK_KEYMAP " @ " QMK_VERSION ", Built on: " QMK_BUILDDATE ", mac:");
            send_string(onMac ? "1" : "0");
        }
    case INVALID: // xxxxxxx
        if (record->event.pressed) {
            notify_ko();
        }
        break;
    case KC_BSPC:
        {
        // Initialize a boolean variable that keeps track
        // of the delete key status: registered or not?
        static bool delkey_registered;
        if (record->event.pressed) {
            // Detect the activation of either shift keys
            if (mod_state & MOD_MASK_SHIFT) {
                // First temporarily canceling both shifts so that
                // shift isn't applied to the KC_DEL keycode
                del_mods(MOD_MASK_SHIFT);
                register_code(KC_DEL);
                // Update the boolean variable to reflect the status of KC_DEL
                delkey_registered = true;
                // Reapplying modifier state so that the held shift key(s)
                // still work even after having tapped the Backspace/Delete key.
                set_mods(mod_state);
                return false;
            }
        } else { // on release of KC_BSPC
            // In case KC_DEL is still being sent even after the release of KC_BSPC
            if (delkey_registered) {
                unregister_code(KC_DEL);
                delkey_registered = false;
                return false;
            }
        }
        }
        break;
    }
    // Let QMK process the keycode as usual
    return true;
}

// Declare a boolean variable to keep track of whether any sequence
// will have been matched.
bool did_leader_succeed;

void matrix_scan_user(void) {

    LEADER_DICTIONARY() {
        did_leader_succeed = leading = false;

        SEQ_ONE_KEY(FR_B) {
            SEND_STRING("Bonne journ");
            tap_code16(FR_EACU);
            SEND_STRING("e");
            did_leader_succeed = true;
        }
        SEQ_ONE_KEY(FR_M) {
            SEND_STRING("Merci d'avance et bonne journ");
            tap_code16(FR_EACU);
            SEND_STRING("e");
            did_leader_succeed = true;
        }
        SEQ_ONE_KEY(GRA_ESC) {
            // `|`
            SEND_STRING(SS_FR_GRAVE SS_FR_GRAVE SS_TAP(X_LEFT));
            did_leader_succeed = true;
        }
        SEQ_TWO_KEYS(GRA_ESC, GRA_ESC) {
            // ```|<enter>```
            SEND_STRING(SS_FR_GRAVE SS_FR_GRAVE SS_FR_GRAVE SS_TAP(X_ENT) SS_TAP(X_ENT) SS_FR_GRAVE SS_FR_GRAVE SS_FR_GRAVE SS_TAP(X_LEFT) SS_TAP(X_LEFT) SS_TAP(X_LEFT) SS_TAP(X_LEFT));
            did_leader_succeed = true;
        }

        SEQ_ONE_KEY(KC_SPC) {
            // mac os > display emoji
            SEND_STRING(SS_DOWN(X_LCTL) SS_DOWN(X_LGUI) SS_TAP(X_SPC) SS_UP(X_LGUI) SS_UP(X_LCTL));
            did_leader_succeed = true;
        }
        leader_end();
    }
}

void leader_end(void) {
    if (did_leader_succeed) {
        notify_ok();
    } else {
        notify_ko();
    }
}

void dance_smile_happy(qk_tap_dance_state_t *state, void *user_data) {
    if (state->count == 1) {
        SEND_STRING(":-) ");
    } else if (state->count == 2) {
        SEND_STRING(";-) ");
    } else if (state->count >= 3) {
        SEND_STRING(":muscle: ");
    }
    reset_tap_dance(state);
}

void dance_smile_cry(qk_tap_dance_state_t *state, void *user_data) {
    if (state->count == 1) {
        SEND_STRING(":-( ");
    } else if (state->count >= 2) {
        SEND_STRING(":sob: ");
    }
    reset_tap_dance(state);
}

void dance_grave(qk_tap_dance_state_t *state, void *user_data) {
    if (state->count == 1) {
        SEND_STRING(SS_TAP(X_3));
    } else if (state->count == 2) {
        SEND_STRING(SS_TAP(X_GRAVE));
    } else if (state->count >= 3) {
        // ```|<enter>```
        SEND_STRING(SS_TAP(X_GRAVE) SS_TAP(X_GRAVE) SS_TAP(X_GRAVE) SS_TAP(X_ENT) SS_TAP(X_ENT) SS_TAP(X_GRAVE) SS_TAP(X_GRAVE) SS_TAP(X_GRAVE) SS_TAP(X_LEFT) SS_TAP(X_LEFT) SS_TAP(X_LEFT) SS_TAP(X_LEFT));
    }
    reset_tap_dance(state);
}

// Tap Dance definitions
qk_tap_dance_action_t tap_dance_actions[] = {
    [TAP_DANCE_HOME] = ACTION_TAP_DANCE_DOUBLE(KC_END, KC_HOME),
    [TAP_DANCE_SMILE_HAPPY] = ACTION_TAP_DANCE_FN(dance_smile_happy),
    [TAP_DANCE_SMILE_CRY] = ACTION_TAP_DANCE_FN(dance_smile_cry),
    [TAP_DANCE_GRAVE] = ACTION_TAP_DANCE_FN(dance_grave),
};

/*
 * RGB LED placement
 * .-------------------- USB -------------------------------------.
 * |     14        15         0         1         2         3     |
 * | 13                                                         4 |
 * |                      View from top                           |
 * | 12                                                         5 |
 * |   11                                      7             6    |
 * '----------\  10                 8   /-------------------------'
 *             \----        9      ----/
 *                   \------------/
 */

void keyboard_post_init_user(void) {
    #if defined(RGBLIGHT_ENABLE)
    // enables Rgb, without saving settings
    rgblight_enable_noeeprom();
    rgblight_mode_noeeprom(RGBLIGHT_MODE_RAINBOW_SWIRL + 2);
    #endif
}
