# Firmware

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).

## Import QMK Source and keymap

```
git clone https://gitlab.com/coliss86/agathe
git clone -b 0.17.5 https://github.com/qmk/qmk_firmware
cd qmk_firmware/keyboards
ln -s ../../agathe/qmk agathe
```

To update `qmk_firmware`
```
cd qmk_firmware
git fetch -a
git checkout <new_tag>
make git-submodule
```

## Flash the Atmega

After setting up your build environment, compile the sources:

```
qmk compile -kb agathe -km default
```

Flash the keyboard:

```
qmk flash -kb agathe -km default
```
