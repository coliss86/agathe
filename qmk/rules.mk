# MCU name
MCU = atmega32u4

# Bootloader selection
#   Teensy       halfkay
#   Pro Micro    caterina
#   Atmel DFU    atmel-dfu
#   LUFA DFU     lufa-dfu
#   QMK DFU      qmk-dfu
#   ATmega32A    bootloadHID
#   ATmega328P   USBasp
BOOTLOADER = atmel-dfu

# Firmware size Optimizations
EXTRAFLAGS += -flto

SLEEP_LED_ENABLE = no  # Breathing sleep LED during USB suspend
NKRO_ENABLE = no      # USB Nkey Rollover - if this doesn't work, see here: https://github.com/tmk/tmk_keyboard/wiki/FAQ#nkro-doesnt-work
ENCODER_ENABLE = no   # Enables the use of one or more encoders

TAP_DANCE_ENABLE = yes  # Enable tad dance
EXTRAKEY_ENABLE = yes       # Audio control and System control

CONSOLE_ENABLE = no # debug, use with #include "print.h", print() and uprint() with qmk toolbox

LTO_ENABLE = yes # Reduce firmware size

# Enable secrets
ifneq ("$(wildcard secrets.c)","")
	SRC += secrets.c
endif

# build with make agathe NO_SECRETS=yes to disable secrets
ifeq ($(strip $(NO_SECRETS)), yes)
    OPT_DEFS += -DNO_SECRETS
endif
