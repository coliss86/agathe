# Agathe ⌨️

An ergonomic 65% Alice clone built with the help of [louckousse](https://github.com/louckousse).

Hardware features:
  - 65% layout
  - Optional 3 rotary encoders
  - RGB underglow
  - Dip switch to change layout between Windows/Linux and Mac to invert the keys <kbd>GUI</kbd> and <kbd>Ctrl</kbd>
  - Custom PCB with a Pro Micro ATmega32U4
  - 3D printed case

![Agathe](doc/agathe.jpg)

Underglow effect:

![Agathe underglow](doc/agathe_underglow.jpg)

Dip switch on the rear side to change layout:

![Agathe DIP](doc/dip.jpg)

### Project structure

* **/case** - `stl`, `step` and `fusion 360` files
* **/qmk** - Firmware folder for QMK
* **/doc** - Pictures, Json files used for [keyboard-layout-editor.com](http://www.keyboard-layout-editor.com/)

## 3D Case 🔧

I designed the case using Fusion 360. Since the bed of my printer (Creality Ender 3) is not large enough, I split the model into parts. I also made sure the split on the plate and the shell are not aligned in order to increase the strength of the case.
I have provided the Fusion 360, STL, and STEP files in the [case](case/) folder.

![Case](doc/case.jpg)

## Electronic & PCB 📟

The PCB was designed with Kicad, the source can be find [here](https://github.com/louckousse/Agathe).

Schematic:

![Schematic](doc/shematic_thumbnail.jpg)

Full res: [PDF](doc/schematic.pdf)

PCB:

![PCB](doc/pcb.jpg)

RGB LED placement

```
.-------------------- USB -------------------------------------.
|     14        15         0         1         2         3     |
| 13                                                         4 |
|                      View from the top                       |
| 12                                                         5 |
|   11                                      7             6    |
'----------\  10                 8   /-------------------------'
            \----        9      ----/
                 \------------/
```

## Layouts 👗👔

3 layouts available:
- [default](qmk/keymaps/default): QWERTY layout with dedicated keys for direct access to most frequent accent characters and a dedicated layer for less used ones. ![Qwerty](doc/layout_default-base.png)
- [azerty](qmk/keymaps/azerty): AZERTY layout. ![Azerty](doc/layout_azerty-base.png)
- [colemak](qmk/keymaps/colemak): [COLEMAK-DH](https://colemakmods.github.io/mod-dh/) layout which sends Azerty code with most frequently used French accents on the center column. See the [documentation of the layout for full details](qmk/keymaps/colemak/README.md). ![Colemak-dh](doc/layout_colemak-base.png)

## Features 🔮

Special features of these layouts:
- [Space cadet shift](https://docs.qmk.fm/#/feature_space_cadet) (Orange Shift Key): tap shift: `{`, hold: `shift`
- [Leader key](https://docs.qmk.fm/#/feature_leader_key) (Purple Leader Key): bunch of shortcuts
- [Tap dance](https://docs.qmk.fm/#/feature_tap_dance): one tap: `:-)`, two taps: `;-)`, three taps: `:muscle:`...
- LED flash when a wrong combo is typed or when a mapping on a layer is `xxxxxxx`: see the [`flash_leds() function` in keymap.c](keymap.c)
- A rotary is used to [adjust the sound volume and when clicked, it acts as a scroll wheel](https://njkyu.com/2021/05/01/rotary-encoder-qmk/)

## BOM 🧾

### Hardware

- [ ] 1 * PCB
- [ ] 1 * USB-C connector to solder
- [ ] 75 * Cherry MX Switches
- [ ] 75 * Keycaps
  - [ ] 62 * 1 U
  - [ ] 3 * 1.25 U
  - [ ] 3 * 1.5 U
  - [ ] 2 * 1.75 U
  - [ ] 2 * 2 U
  - [ ] 2 * 2.25 U
  - [ ] 1 * 2.75 U
- [ ] 5 * Stabs 2 U

Optional:
- [ ] Up to 3 encoders (optional) with knob
- [ ] Dip switch
- [ ] 3 * M3x16 screw (see issue below)

### 3D printed parts

Stl files are available in the [case](case/) folder:
- [ ] 1 * Shell Center
- [ ] 1 * Shell Left
- [ ] 1 * Shell Right
- [ ] 1 * Plate Left
- [ ] 1 * Plate Right
- [ ] 20 * Ergo
- [ ] 3 * Cache rotary (optional)

I used translucent PLA for the shell and bling PLA for the plate.

## Issue 🚩

Holes are missing in the PCB to fix the plate to the shell. 3 of them with a diameter of 3 mm can be drilled at the position surrounded by a yellow circle in the schematic below:

![PCB holes](doc/pcb_holes.jpg)

3 screws M3*16 can then be used to secure the plate to the shell.

## Useful links 🔗

- [Tap dance basics](https://thomasbaart.nl/2018/12/13/qmk-basics-tap-dance/)
- [Home row explanation + advanced layer switch](https://precondition.github.io/home-row-mods)
- [Learn Colemak](https://www.colemak.academy/)
- [3D printed hotswap](https://github.com/stingray127/handwirehotswap)
- [Key code full list](https://beta.docs.qmk.fm/using-qmk/simple-keycodes/keycodes)
- [Keyboard layout editor](http://www.keyboard-layout-editor.com/)
- [Plate & case builder](http://builder.swillkb.com/)
